#!/bin/bash

if [ $UID != 0 ]; then
 echo "No tienes los privilegios necesarios para ejecutar este script."
 echo "Debes ingresar como root, escribe \"su root\" sin las comillas."
 exit 1
fi

while [ "$OPT" != "0" ]
 do
 clear
  echo
  echo "Gestion de Usuarios:"
  echo "---------"
  echo " 1. Agregar un usuario al sistema."
  echo " 2. Cambiar la clave de acceso de un usuario."
  echo " 3. Editar la información personal de un usuario."
  echo " 4. Borrar a un usuario del sistema."
  echo " 5. Crear grupo."
  echo " 6. Agregar un usuario a un grupo."
  echo " 7. Borrar un grupo."
  echo " 8. Obtener listado de usuarios "
  echo " 9. obtener listado de grupos."
  echo " 0. Salir."
  echo ""
  echo -n " Ejecutar: "
  read OPT

   case $OPT in
    1 )
     echo
     echo -n "Nombre del usuario a crear?: "
     read USUARIO
     echo "Con acceso a la línea de comandos bash [y/n]?"
     echo -n "Respuesta: "
     read ACCESO
     if [[ "$ACCESO" == "y" || "$ACCESO" == "Y" ]]; then
      TERMINAL='/bin/bash'
     elif [[ "$ACCESO" == "n" || "$ACCESO" == "N" ]]; then
      TERMINAL='/bin/false'
     fi
     echo "Crear el directorio de trabajo para el nuevo usuario [y/n]?"
     echo -n "Respuesta: "
     read DIRECTORIO
     if [[ "$DIRECTORIO" == "y" || "$DIRECTORIO" == "Y" ]]; then
      useradd $USUARIO -s $TERMINAL
     elif [[ "$DIRECTORIO" == "n" || "$DIRECTORIO" == "N" ]]; then
      useradd $USUARIO -m -s $TERMINAL
     fi
     echo
    ;;

    2 )
     echo
     echo -n "Nombre del usuario del que se desea cambiar la clave?: "
     read USUARIO
     echo
     passwd $USUARIO
     echo
    ;;

    3 )
     echo
     echo -n "Nombre del usuario al cual cambiar la información personal?: "
     read USUARIO
     echo
     chfn $USUARIO
     echo
    ;;

    4 )
     echo
     echo -n "Nombre del usuario a borrar?: "
     read USUARIO
     if [ "$USUARIO" = "root" ]; then
      echo "Este Script no puede eliminar el usuario root"
      exit 0
     else
      echo
      echo "Desea borrar el directorio de trabajo y todo su contenido [y/n]? "
      echo -n "Respuesta: "
      read BORRARDIR
     fi
     if [[ "$BORRARDIR" == "y" || "$BORRARDIR" == "Y" ]]; then
      userdel -r $USUARIO
     elif [[ "$BORRARDIR" == "n" || "$BORRARDIR" == "N" ]]; then
      userdel $USUARIO
     fi
     echo
    ;;

    5 )
     echo
     echo -n "Nombre del grupo a crear?: "
     read GRUPO
     echo
     groupadd $GRUPO
     echo
    ;;

    6 )
     echo
     echo -n "Nombre del usuario para agregar al grupo?: "
     read USUARIO
     echo -n "Nombre del del grupo para agregar el usuario?: "
     read GRUPO
     echo
     usermod -g $GRUPO $USUARIO
     echo
    ;;

    7 )
     echo -n "Nombre del grupo a borrar?: "
     read GRUPO
     echo
     groupdel $GRUPO
     echo
    ;;

    8 )
     echo ""
     echo -n "Listado de usuarios cargados"
     echo ""
     cat /etc/passwd | grep -f /etc/shells | cut -d: -f1
     echo ""
     echo " ----------------------------- "
     echo ""
     echo " Usuarios de sistemas y personales y sus correspondientes directorios "
     cat /etc/passwd | cut -d: -f 1,6
     echo ""
    ;;

    9 )
     echo ""
     echo -n "Listado de grupos cargados"
     echo ""
     cat /etc/group
     echo ""
     ;;

    *)
    ;;
   esac
 read -p "presione enter para continuar"
 done
