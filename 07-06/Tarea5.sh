#!/bin/bash

clear
OPT=0
while [ "$OPT" -ne "5" ]; do
      echo "Configuraciones de RED"
      echo ""
      echo "1. Calculadora Red "
      echo ""
      echo "2. Ver Interfaz de Red Activa "
      echo ""
      echo "3. Ver configuración de Red General"
      echo ""
      echo "4. Verificar puertos abiertos"
      echo ""
      echo "5. Salir "
      echo ""
      echo -n "Selecciona una opción": 
      read OPT

case $OPT in

1) clear
	echo "Para la ejecucion del script necesitamos el paquete IPCALC"
	read -p "Desea verificar si esta instalado? [y/n]: " OPCION
	echo ""
	if [[ $OPCION == "y" || $OPCION == "Y"  ]]; then
		dpkg -s ipcalc | grep ipcalc
		echo ""
		read -p "Esta instalado? [y/n]: " ANS
		echo ""
		if [[ $ANS == "y" || $ANS == "Y" ]]; then
			read -n 20 -p "Ingrese la ip que desea subnetear:" RED
                 	read -n 3 -p "Ingresa la mascara de subred que desea (Con o sin /): " SUB
			echo "La red es :" && echo "$RED con la subred"/$SUB""
                  	echo ""
                  	ipcalc $RED $SUB
		elif [[ $ANS == "n" || $ANS == "N"  ]]; then
			echo "Instalando..."
			sleep 2s
			sudo apt-get install ipcalc || sudo yum install ipcalc
			clear
			read -n 20 -p "Ingrese la ip que desea subnetear: " RED
			read -n 3 -p "Ingresa la mascara de  subred que desea (Con o sin /): " SUB
			echo "La red es :" && echo "$RED con la subred "/$SUB""
			echo ""
			ipcalc $RED $SUB
		fi
		else
			echo "No podra calcular su subred"

	fi
	;;

2) clear
   echo "Listamos las interfaces de Red Activas"
   echo ""
   networkctl status
   sleep 3s
   echo ""
   ;;

3) clear
   echo "IPv4 e IPv6"
   ifconfig | grep inet
   echo ""
   echo "---------------------"
   echo ""
   echo "MAC"
   ifconfig | grep ether
   echo ""
   echo "---------------------"
   echo ""
   ifconfig | grep wlan
   echo ""
   echo "---------------------"
   echo ""
   echo "Servidores DNS"
   cat /etc/resolv.conf
   echo ""
   echo "---------------------"
   echo ""
   echo "Puerta de Enlace"
   ip route show | grep default
   echo ""
   echo "---------------------"
   ;;

4) clear
	echo "Para la ejecucion del script necesitamos el paquete NMAP"
        read -p "Desea verificar si esta instalado? [y/n]: " OPCION
        echo ""
        if [[ $OPCION == "y" || $OPCION == "Y"  ]]; then
                dpkg -s nmap | grep nmap
                echo ""
                read -p "Esta instalado? [y/n]: " ANS
                echo ""
                if [[ $ANS == "y" || $ANS == "Y" ]]; then
 			echo "Verificando puertos disponibles"
 			echo ""
                        nmap localhost
                elif [[ $ANS == "n" || $ANS == "N"  ]]; then
                        echo "Instalando..."
                        sleep 2s
                        sudo apt-get install nmap || sudo yum install nmap
			echo "Verificando puertos disponibles"
			echo ""
                        nmap localhost
                fi
                else
                        echo "No podra verificar puertos"

        fi
        ;;

5) clear
   echo ""
   echo "Saliendo de la aplicación"
   echo ""
   sleep 1s
   exit
   ;;

*) clear
   echo "No estas introduciendo ninguna opción valida"
   echo ""
   echo "Volve a intentarlo"
   echo "" ;;
esac
echo ""
sleep 1s
read -p "Presione enter para continuar..."
clear
done
