#!/bin/bash

echo "Bienvenido al asistente de nuevo usuario"

echo " "

echo "revisando privilegios"

sleep 2s

echo " "

if [ "$EUID" -ne 0 ]; then
	echo "Usted no posee privilegios de administrador"
	echo " "
	echo "Conatctese con el personal de sistemas para ejecutar el script"
	exit 1
fi

read -p "Igrese nombre de usuario: " USUARIO

useradd -s /bin/bash -d /home/$USUARIO -m $USUARIO

echo " "

read -p "presione enter para ingresar la contraseña de $USUARIO"

echo " "

passwd $USUARIO

echo "a que area correponde $USUARIO?"

echo " "

echo "1) Administracion"

echo "2) Sistema"

echo "3) Salir"

echo " "

read -p "Seleccione el area correspondiente: " OPT

case $OPT in

	1) echo "El area administrativa requiere de la siguiente aplicacion: Calculadora"
	read -p "dsea instalarla? " ANS
	if [[ $ANS == "y" || $ANS == "Y" ]]; then
		apt install bc -y
	else
		echo "finalizando"
		exit 0
	fi

	;;

	2) echo "El area sistemas requiere de la siguiente aplicacion: vim"
	read -p "dsea instalarla? " ANS
        if [[ $ANS == "y" || $ANS == "Y" ]]; then
                apt install vim -y
        else
                echo "finalizando"
                exit 0
        fi

        ;;

	3) echo "Saliendo del asistente"

	sleep 1s

	exit 0

	;;

esac

sleep 2s

echo " "

echo "Generando reporte de usuario"

cat /etc/passwd | grep $USUARIO | cut -d: -f 1 > /tmp/infusuario.txt

sleep 1s

echo "Reporte generado exitosamente"

echo " "

echo "Finalizacion del asistente de creacion de usuario"

sleep 1s
