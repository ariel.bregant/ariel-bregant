#!/bin/bash

clear

# Check root user
if [ "$EUID" -ne 0 ]; then
	echo "Solo podra ejecutar este programa como Administrador"
	echo "Saliendo..."
	echo ""
	exit 1
fi

# Funciones 
updateByVersion() {
	if [[ $1 == "Ubuntu" || $1 == "Debian" ]]; then
		echo "Actualizando..."
		sudo apt-get update -y && sudo apt-get upgrade -y
	elif [ $1 == "CentOS" ]; then	
		echo "Actualiando..."
		sudo yum update -y && sudo yum upgrade -y
	fi
}

checkPaquete() {
	if cat /tmp/$1 | grep -i $2; then
		echo ""
		echo "Paquete existente"
	else 
		echo ""
		echo "Paquete no existente, no podra ejecutar ninguna accion"
		exit 1
	fi
}

echo "-------------------------------------"
echo "--------Gestion de Paquetes----------"
echo "-------------------------------------"
echo "1) Actualizar paquetes y repositorios"
echo "2) Instalar Paquetes"
echo "3) Chequear existencia de paquetes"
echo "4) Eliminar Paquetes"
echo "5) Vaciar el cache"
echo "6) Eliminar dependencias obsoletas" 
echo "7) Salir"
echo ""
read -p "Seleccione una opcion: " OPT

case $OPT in

	1)
		echo ""
		echo "Para continuar, necesitamos chequear su distribucion"
		echo "Chequeando..."
		sleep 1
		lsb_release -d
		read -p "Que distribucion posee? (Indiquelo tal cual se muestra en la pantalla): " OS
	       	echo ""
		updateByVersion "$OS"
		;;

	2)
		echo ""
		read -p "Que paquete desea instalar: " OPCION
		sudo apt-get install $OPCION -y
		;;

	3)
                echo ""
                echo "Le mostraremos y exportaremos un archivo temporal con sus paquetes actuales"
                read -p "Como desea llamar al archivo temporal? " NAME
		echo "Exportando..."
		sleep 2
		apt list --installed | less > /tmp/$NAME && apt list --installed | less
		echo "No vio su paquete en la lista y desea asegurarse?"
		echo "A continuacion, filtraremos el archivo exportado"
		read -p "Indique el nombre del paquete a buscar: " PACKAGE
		echo ""
		checkPaquete "$NAME" "$PACKAGE"
		;;


	4)
		echo ""
		read -p "Indique el nombre del paquete a eliminar: " PAQUETE
		echo "Validaremos la existencia del mismo, con el archivo exportado anteriormente"
		read -p "Como nombro al archivo?: " NAME
		echo ""
		checkPaquete "$NAME" "$PAQUETE"
		echo ""
		echo "Eliminando..."
		sleep 1
		apt-get purge $PAQUETE
		;;

	5)
		echo ""
		echo "Esta accion no se podra revertir"
		read -p "Desea continuar? [y/n] " ANS
		if [[ $ANS == "Y" || $ANS == "y" ]]; then
			echo "Limpiando cache..."
			sleep 2
			sudo apt-get autoclean -y ; sudo apt-get clean -y
		else
			echo ""
			echo "No se realizara ninguna accion"
		fi
		;;

	6)
		echo ""
		echo "Esta accion podria provocar que un programa deje de funcionar"
		read -p "Desea continuar? [y/n] " RES
		if [[ $RES == "Y" || $RES == "y" ]]; then
			echo "Ejecutando.."
			sleep 2
			sudo apt-get autoremove
		else
			echo ""
			echo "No se realizara ninguna accion"
			exit 0
		fi
		;;

	7)
		echo ""
		echo "Gracias por utilizar el asistente" 
		echo "Saliendo.."
		sleep 1
		exit 0
		;;
esac

